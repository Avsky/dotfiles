#!/bin/bash

DF_ROOT=$(realpath "$(dirname "$0")")

echo "Dotfiles path: ${DF_ROOT}"

install_symlink() {
	DF_TARGET="${DF_ROOT}/$1"
	DF_LINK="$2"

	if [[ ! -f ${DF_TARGET} ]]; then
		echo "ERR: ${DF_TARGET} is not a file, skipping"
		return
	fi

	if [[ -L ${DF_LINK} ]]; then
		CURRENT_TARGET=$(realpath "${DF_LINK}")
		if [[ "${CURRENT_TARGET}" == "${DF_TARGET}" ]]; then
			echo "OK: ${DF_LINK} already points to ${DF_TARGET}"
		else
			echo "SKIP: ${DF_LINK} already exists and points to ${CURRENT_TARGET}"
		fi

		return
	fi

	if [[ -f ${DF_LINK} ]]; then
		echo "SKIP: ${DF_LINK} already exists"
		return
	fi

	echo "OK: linking ${DF_LINK} to ${DF_TARGET}"
	ln -s "${DF_TARGET}" "${DF_LINK}"
}

install_symlink "zsh/zshrc" "${HOME}/.zshrc"
install_symlink "zsh/zimrc" "${HOME}/.zimrc"

echo "Done symlinking"
